<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnForMedia extends Migration
{
    protected $table = "media";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $hasTable = Schema::hasTable($this->table);

        if ($hasTable) {
            Schema::table($this->table, function (Blueprint $table) {
                if (!Schema::hasColumn($this->table, "publish")) {
                    $table->tinyInteger('publish')->default(0)->comment("0:no,1:yes")->after("size_in_kb");
                }

                if (!Schema::hasColumn($this->table, "sequence")) {
                    $table->bigInteger('sequence')->default(0)->after("publish");
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $hasTable = Schema::hasTable($this->table);

        if ($hasTable) {
            Schema::table($this->table, function (Blueprint $table) {
                if (Schema::hasColumn($this->table, "publish")) {
                    $table->dropColumn('publish');
                }

                if (Schema::hasColumn($this->table, "sequence")) {
                    $table->dropColumn('sequence');
                }
            });
            
        }
    }
}
