<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


/* CMS BackOffice Routes */

Route::group(['prefix' => 'cms', 'namespace' => 'cms'], function () {
    Route::get('dashboard', 'HomeController@index')->name('dashboard');

    Route::group(['name' => 'blog.'], function () {
        Route::resource('blogs', 'BlogController');
        Route::post("ckeditor/image/upload", "BlogController@uploadCKImage")->name("ckeditor.upload.image");
    });

    Route::resource('seo', 'SeoController');
    Route::resource('banner', 'BannerController');

    Route::resource("gallery", "AlbumController");

    Route::prefix("gallery/{gallery}/images")->name("gallery.images")->group(function () {
        Route::post("store", "AlbumController@uploadImage")->name(".store");
        Route::put("{media}", "AlbumController@updateImage")->name(".update");
        Route::delete("{media}", "AlbumController@destroyImage")->name(".destroy");
    });
});

/* ---------- CMS BackOffice Routes End ---------- */
