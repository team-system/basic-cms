<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function home()
    {
        return view('web.home');
    }

    public function about()
    {
        return view('web.aboutus');
    }

    
    public function explore()
    {
        return view('web.explore');
    }

    
    public function directory()
    {
        return view('web.directory');
    }

    
    public function promotion()
    {
        return view('web.promotion');
    }

    
    public function event()
    {
        return view('web.event');
    }

    
    public function contact()
    {
        return view('web.contactus');
    }
}
