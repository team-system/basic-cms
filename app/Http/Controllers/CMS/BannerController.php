<?php

namespace App\Http\Controllers\CMS;

use App\Banner;
use Validator, File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = Banner::where('active', 1)->get();

        return view('CMS.banner', ['results' => $results]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
			'file.*' => 'required|file|max:10240|mimes:jpeg,jpg,png',
        ]);
        
		$validator->setAttributeNames([
			'file.*' => 'File',
		]);
    
		if($validator->fails()) {
			return response($validator->messages()->all());
		}
		
		foreach($request->file("file") as $file)
		{
            $file_name = str_random(20);
            $ext = $file->getClientOriginalExtension();
            $destination_path = 'images/banner/';
            $file->move($destination_path,$file_name.'.'.$ext);
            $file_url = $destination_path.$file_name.'.'.$ext;

            $last_seq = (Banner::get()->count() != 0) ? Banner::orderBy('seq','desc')->first()->seq : 0;
			
            $banner = new Banner();
            $banner->img_url = $file_url;
            $banner->seq = $last_seq + 1;
			$banner->save();
        }
		
        return response(200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
			'link' => 'url',
        ]);
        
		$validator->setAttributeNames([
			'file' => 'Link',
		]);
    
		if($validator->fails()) {
            return redirect()->back()->withErrors($validator, "formerror");
        }
        
        $banner = Banner::find($id);

        $current_seq = $banner->seq;
        $target_seq = $request->input('seq');

        if($current_seq < $target_seq) {
            $banner_higher_seq = Banner::where('seq', '>', $current_seq)->where('seq', '<=', $target_seq)->get();
            foreach($banner_higher_seq as $banner_higher){
                $banner_higher->seq -= 1;
                $banner_higher->save();
            }
        } elseif ($current_seq > $target_seq) {
            $banner_lower_seq = Banner::where('seq', '<', $current_seq)->where('seq', '>=', $target_seq)->get();
            foreach($banner_lower_seq as $banner_lower){
                $banner_lower->seq += 1;
                $banner_lower->save();
            }
        }

        $banner->seq = $target_seq;
        $banner->title = $request->input('title');
        $banner->desc = $request->input('desc');
        $banner->link = $request->input('link');
        $banner->save();

        return redirect()->back()->withSuccess('Banner has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $current_seq = Banner::find($id)->seq;
        Banner::where('id', $id)->delete();

        $banner_higher_seq = Banner::where('seq', '>', $current_seq)->get();
        foreach($banner_higher_seq as $banner_higher){
            $banner_higher->seq -= 1;
            $banner_higher->save();
        }

        return redirect()->back()->withSuccess('Banner has been deleted.');
    }
}
