<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\Blog;


class BlogController extends Controller
{
    protected $upload_path = '/app/public/blog_media';
    protected $asset_path = '/storage/blog_media/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::get();

        return view('CMS.blog.index',compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('CMS.blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            "title" => "required|string",
            "content" => "required|string"
        ]);
        $title = $request->get('title');
        $newSlug = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", strtolower($title)));
        $slugs = Blog::where('slug',$newSlug)->get();
        if(!empty($slugs)){
            $count = 1;
            foreach($slugs as $slug){
                if($slug->slug != $newSlug.'-'.$count){
                    $newSlug = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", strtolower($title))).'-'.$count;
                }
                $count++;
            }
        }
        $blog = (new Blog)->storeRecord([
            "title" => $title,
            "slug" => $newSlug,
            "content" => $request->get('content'),
            "status" => 1,
            "written_by" => 0
        ],true);

        session()->flash('success','New Blog Created.');
        return redirect()->route('blogs.show',['blog'=> $blog->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        return view('CMS.blog.show',compact('blog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        return view('CMS.blog.edit',compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        $this->validate($request, [
            "title" => "required|string",
            "content" => "required|string"
        ]);
        $title = $request->get('title');
        $data = [
            "content" => $request->get('content')
        ];
        if($blog->title != $title){
            $newSlug = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", strtolower($title)));
            $slugs = Blog::where('slug',$newSlug)->get();
            if(!empty($slugs)){
                $count = 1;
                foreach($slugs as $slug){
                    if($slug->slug != $newSlug.'-'.$count){
                        $newSlug = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", strtolower($title))).'-'.$count;
                    }
                    $count++;
                }
            }
            $data['slug'] = $newSlug;
            $data['title'] = $title;
        }
        $result = $blog->updateRecord($data,true);

        session()->flash('success','Blog Updated.');
        return redirect()->route('blogs.show',['blog'=> $result->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function uploadCKImage(Request $request) {

        $this->upload_path = storage_path().$this->upload_path;

        $validator = Validator::make($request->toArray(), [
                    "upload" => "required|file|image|max:2048"
                        ], [
                    "upload.required" => "Please select an image to upload.",
                    "upload.file" => "Please upload an image.",
                    "upload.image" => "Invalid file type. Please upload image type only. (jpg, jpeg, png, bmp and svg)",
                    "upload.max" => "Upload limit exceeded, please upload the image within 2MB."
        ]);


        if ($validator->fails()) {
            return response()->json([
                        "uploaded" => 0,
                        "error" => [
                            "message" => $validator->errors()->first()
                        ]
            ]);
        }

        if ($request->hasFile("upload")) {

            $file = $request->file("upload");
            $extension = strtolower($file->getClientOriginalExtension());
            $file_name = "media_".date("Ymdhis").rand(11,99).'.'.$extension;
            $upload = $file->move($this->upload_path,$file_name);

            if($upload){
                $url = asset($this->asset_path.$file_name);
                return response()->json([
                            "uploaded" => 1,
                            "fileName" => $file_name,
                            "url" => $url
                ]);
            }else {
                return response()->json([
                            "uploaded" => 0,
                            "error" => [
                                "message" => "Fail to upload image."
                            ]
                ]);
            }
        }
    }
}
