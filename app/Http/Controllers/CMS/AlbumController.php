<?php

namespace App\Http\Controllers\CMS;

use App\Models\Album;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Media;

class AlbumController extends Controller
{
    protected $folder = "CMS.albums";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $albums = Album::get();

        return view($this->folder . ".index", compact('albums'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->folder . ".create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->toArray(), [
            "title" => "required|string|max:191",
            "description" => "nullable|string"
        ]);

        if ($validate->fails()) {
            return redirect()->back()->withInput()->withErrors($validate);
        }

        $album = new Album();
        $album->name = $request->get("title");
        $album->description = $request->get("description");
        $album->publish = $request->has("status");
        $album->save();

        return redirect()->route("gallery.index")->withSuccess('Ablum has been created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function show(Album $gallery)
    {
        $album = Album::with([
            "galleries" => function ($query) {
                $query->orderBy("sequence", "asc");
            }
        ])->where("id", $gallery->id)
            ->first();

        return view($this->folder . ".show", compact('album'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function edit(Album $gallery)
    {
        $album = $gallery;
        return view($this->folder . ".edit", compact('album'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Album $gallery)
    {
        $album = $gallery;
        $validate = Validator::make($request->toArray(), [
            "title" => "required|string|max:191",
            "description" => "nullable|string"
        ]);

        if ($validate->fails()) {
            return redirect()->back()->withInput()->withErrors($validate);
        }

        $album->name = $request->get("title");
        $album->description = $request->get("description");
        $album->publish = $request->has("status");
        $album->save();

        return redirect()->route("gallery.index")->withSuccess('Ablum has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function destroy(Album $gallery)
    {
        if (!empty($gallery)) {
            $gallery->galleries()->delete();
            $gallery->delete();
        }

        session()->flash("success", "Album has been deleted.");
        return response()->json([
            "deleted" => 1,
            "message" => 'Album has been deleted.'
        ]);
    }

    public function uploadImage(Request $request, Album $gallery)
    {
        $validator = Validator::make($request->all(), [
            'file.*' => 'required|file|max:10240|mimes:jpeg,jpg,png',
        ]);

        $validator->setAttributeNames([
            'file.*' => 'File',
        ]);

        if ($validator->fails()) {
            return response($validator->messages()->all());
        }

        foreach ($request->file("file") as $file) {
            $file_name = str_random(20);
            $ext = $file->getClientOriginalExtension();
            $file_original_name = $file->getClientOriginalName();
            $file_size = $file->getSize();
            $file_mime_type = $file->getMimeType();
            $destination_path = Album::DESTINATION_PATH;
            $file->move($destination_path, $file_name . '.' . $ext);
            $file_url = $destination_path . $file_name . '.' . $ext;

            $media = new Media();
            $media->parent_type = Album::class;
            $media->parent_id = $gallery->id;
            $media->path = $file_url;
            $media->original_name = $file_original_name;
            $media->extension = $ext;
            $media->mime = $file_mime_type;
            $media->size_in_kb = $file_size;
            $media->save();
        }

        return response(200);
    }

    public function updateImage(Request $request, Album $gallery, Media $media)
    {
        $media->sequence = $request->get("seq", $media->sequence);
        $media->publish = $request->has("status");
        $media->save();

        return redirect()->back()->withSuccess('Gallery Image has been updated.');
    }

    public function destroyImage(Request $request, Album $gallery, Media $media)
    {
        if (!empty($media)) {
            $media->delete();
        }

        return redirect()->back()->withSuccess('Gallery Image has been deleted.');
    }
}
