<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator, Storage;
use App\Models\Seo;

class SeoController extends Controller
{

    protected $upload_path = '/seo_media/';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $seo_list = Seo::get();

        return view('CMS.seo.index',compact('seo_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('CMS.seo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            "page_url" => "required|string",
            "title" => "required|string",
            "description" => "required|string",
            "type" => "string",
            "keywords" => "string",
            "image" => "image|max:7000"
        ]);
        
        $seo = (new Seo)->storeRecord([
            "page_url" => $request->get('page_url'),
            "title" => $request->get('title'),
            "description" => $request->get('description'),
            "type" => $request->get('description'),
            "keywords" => $request->get('keywords'),
        ],true);

        if ($request->hasFile("image")) {

            $file = $request->file("image");
            $extension = strtolower($file->getClientOriginalExtension());
            $file_name = "media_".date("Ymdhis").rand(11,99).'.'.$extension;
            
            $upload = Storage::disk('public')->put($this->upload_path.$file_name, file_get_contents($file->getRealPath()));

            if($upload){

                $result = $seo->updateRecord([
                    "image" => Storage::url($this->upload_path.$file_name)
                ],true);

            }else {
                session()->flash('success','New SEO Created, but image is failed to upload.');
                return redirect()->route('seo.show',['seo'=> $seo->id]);
            }
        }

        session()->flash('success','New SEO Created.');
        return redirect()->route('seo.show',['seo'=> $seo->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Seo  $seo
     * @return \Illuminate\Http\Response
     */
    public function show(Seo $seo)
    {
        return view('CMS.seo.show',compact('seo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Seo  $seo
     * @return \Illuminate\Http\Response
     */
    public function edit(Seo $seo)
    {
        return view('CMS.seo.edit',compact('seo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Seo  $seo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Seo $seo)
    {
        $this->validate($request, [
            "page_url" => "required|string",
            "title" => "required|string",
            "description" => "required|string",
            "type" => "string",
            "keywords" => "string",
            "image" => "image|max:7000"
        ]);
        
        $result = $seo->updateRecord([
            "page_url" => $request->get('page_url'),
            "title" => $request->get('title'),
            "description" => $request->get('description'),
            "type" => $request->get('description'),
            "keywords" => $request->get('keywords')
        ],true);

        if ($request->hasFile("image")) {

            $file = $request->file("image");
            $extension = strtolower($file->getClientOriginalExtension());
            $file_name = "media_".date("Ymdhis").rand(11,99).'.'.$extension;
            
            $upload = Storage::disk('public')->put($this->upload_path.$file_name, file_get_contents($file->getRealPath()));

            if($upload){

                $result = $seo->updateRecord([
                    "image" => Storage::url($this->upload_path.$file_name)
                ],true);

            }else {
                session()->flash('success','SEO Updated, but image is failed to upload.');
                return redirect()->route('seo.show',['seo'=> $seo->id]);
            }
        }

        session()->flash('success','SEO Updated.');
        return redirect()->route('seo.show',['seo'=> $result->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Seo  $seo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Seo $seo)
    {
        $seo->delete();

        return response()->json([
            "deleted" => 1,
            "message" => 'SEO deleted'
        ]);
    }
}
