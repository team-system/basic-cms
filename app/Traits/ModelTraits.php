<?php

namespace App\Traits;

// use App\Constants\DNA;
use App\Models\Media;
use Exception;
use Illuminate\Database\Eloquent\Builder;

trait ModelTraits
{

    /** [OVERWRITE]
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope("orderBy", function (Builder $builder) {
            $builder->orderBy((new static)->getTable() . ".created_at", "DESC");
        });
    }

    /** [SCOPE]
     */
    // public function scopeActive($query, $column = "status")
    // {
    //     return $query->where($column, DNA::STATUS_ACTIVE);
    // }

    // public function scopeInactive($query, $column = "status")
    // {
    //     return $query->where($column, DNA::STATUS_INACTIVE);
    // }

    /** [ATTRIBUTE]
     */
    public function getPublishTextAttribute()
    {
        return (!empty($this->publish) && isset($this->publish)) ? "Publish" : "Unpublish";
    }

    public function getCreatedAtFormattedAttribute()
    {
        try {
            return (is_null($this->created_at)) ? _now() : _parse($this->created_at);
        } catch (Exception $ex) {
            _systemLog($ex, "error");
        }

        return _now();
    }

    public function getUpdatedAtFormattedAttribute()
    {
        try {
            return (is_null($this->updated_at)) ? _now() : _parse($this->updated_at);
        } catch (Exception $ex) {
            _systemLog($ex, "error");
        }

        return _now();
    }

    public function getDeletedAtFormattedAttribute()
    {
        try {
            return (is_null($this->deleted_at)) ? null : _parse($this->deleted_at);
        } catch (Exception $ex) {
            _systemLog($ex, "error");
        }

        return _now();
    }

    /** [FUNCTION]
     */
    public function getMediaAssetPath($name)
    {
        return route("media.view-profile-image", $name);
    }

    /**
    |--------------------------------------------------------------------------
    | Store Record Function
    |--------------------------------------------------------------------------
     */
    public function storeRecord(array $data, $return = false, array $morph = null)
    {

        try {
            if (is_array($data)) {

                $count = 0;
                foreach ($data as $key => $val) {
                    $this->$key = $val;
                    $count++;
                }

                if ($count > 0) {
                    if (!empty($morph)) {
                        $morph['parent']->{$morph['child']}()->save($this);
                    } else {
                        $this->save();
                    }
                }

                return ($return) ? $this : true;
            }
        } catch (Exception $ex) {
            dd($ex);
            _systemLog($ex, "error");
        }

        return false;
    }

    /**
    |--------------------------------------------------------------------------
    | Update Record Function
    |--------------------------------------------------------------------------
     */
    public function updateRecord(array $data, $return = false, $morph = false)
    {

        try {
            if (is_array($data)) {

                $count = 0;
                foreach ($data as $key => $val) {
                    $this->$key = $val;
                    $count++;
                }

                if ($count > 0) {
                    if (!empty($morph)) {
                        $morph['parent']->{$morph['child']}()->save($this);
                    } else {
                        $this->save();
                    }
                }

                return ($return) ? $this : true;
            }
        } catch (Exception $ex) {
            _systemLog($ex, "error");
        }

        return false;
    }

    /**
    |--------------------------------------------------------------------------
    | Destroy Record Function
    |--------------------------------------------------------------------------
     */
    public function destroyRecord(array $data)
    { }

    /**
    |--------------------------------------------------------------------------
    | Store Record Function (Advanced)
    |--------------------------------------------------------------------------
     */
    public function storeRecordAdvanced(array $data, $extra = false)
    {
        try {
            if (is_array($data)) {

                $count = 0;
                $encrypt = !empty($field['encrypt']) ? $field['encrypt'] : false;
                $bcrypt = !empty($field['bcrypt']) ? $field['bcrypt'] : false;
                $file = !empty($field['file']) ? $field['file'] : false;

                if ($extra) {
                    foreach ($data as $key => $field) {
                        if ($encrypt === true) {
                            $this->$key = encrypt($field['value']);
                        } elseif ($bcrypt === true) {
                            $this->$key = bcrypt($field['value']);
                        } elseif ($file === true) {
                            $morph = is_array($field['morph']) ? $field['morph'] : [
                                "parent" => (object)[],
                                "child" => (object)[]
                            ];

                            $result = $this->fileUploads($morph, $field['value'], $ip);
                        } else {
                            $this->$key = $field['value'];
                        }
                        $count++;
                    }
                } else {
                    foreach ($data as $key => $val) {
                        $this->$key = $val;
                        $count++;
                    }
                }

                if ($count > 0) {
                    $this->save();
                }

                return true;
            }
        } catch (Exception $ex) {
            _systemLog($ex, "error");
        }

        return false;
    }

    /**
    |--------------------------------------------------------------------------
    | Update Record Function (Advanced)
    |--------------------------------------------------------------------------
     */
    public function updateRecordAdvanced(array $data, $extra = false)
    {

        try {
            if (is_array($data)) {

                $count = 0;

                if ($extra) {
                    foreach ($data as $key => $field) {
                        $encrypt = !empty($field['encrypt']) ? $field['encrypt'] : false;
                        $bcrypt = !empty($field['bcrypt']) ? $field['bcrypt'] : false;
                        $file = !empty($field['file']) ? $field['file'] : false;

                        // dd($file);
                        if ($encrypt === true) {
                            $this->$key = encrypt($field['value']);
                        } elseif ($bcrypt === true) {
                            $this->$key = bcrypt($field['value']);
                        } elseif ($file === true) {
                            $file_type = !empty($field['file_type']) ? $field['file_type'] : 'BACKGROUND';
                            $ip = !empty($field['ip']) ? $field['ip'] : null;
                            $result = $this->fileUploads($file_type, $field['value'], $ip);

                            // if($result !== false ){
                            //     $this->$key = $result;
                            // }
                        } else {
                            $this->$key = $field['value'];
                        }
                        $count++;
                    }
                } else {
                    foreach ($data as $key => $val) {
                        $this->$key = $val;
                        $count++;
                    }
                }

                if ($count > 0) {
                    $this->save();
                }

                return true;
            }
        } catch (Exception $ex) {
            _systemLog($ex, "error");
        }

        return false;
    }

    /**
    |--------------------------------------------------------------------------
    | File Upload Function
    |--------------------------------------------------------------------------
     */
    public function fileUploads(array $morph, $file, $ip = null)
    {

        if (!is_file($file)) {
            return false;
        }

        $path = Media::NEW_FILE_PATH;

        $originalName = $file->getClientOriginalName();
        $mime = $file->getClientMimeType();
        $size = $file->getClientSize();
        $extension = strtolower($file->getClientOriginalExtension());

        $file_name = strtolower($type) . "_" . date("Ymdhis") . rand(11, 99);
        $upload = $file->move($path, $file_name . '.' . $extension);
        if ($upload) {
            Media::deleteRecordByObjectIdAndType($this->id, $objectType);
            $media = new Media;
            $media->storeRecord([
                "name" => $file_name,
                "original_name" => $originalName,
                "extension" => $extension,
                "mime" => $mime,
                "size" => $size,
                "path" => $path,
                // "ip_address" => $ip,
            ], false, $morph);
            return true;
        }
        return false;
    }

    /**
    |--------------------------------------------------------------------------
    | Check Image Function
    |--------------------------------------------------------------------------
     */
    public function checkImageExists()
    {
        try {
            if (!empty($this->name)) {
                $explode = explode(".", $this->name);
                $name = isset($explode[0]) ? $explode[0] : $this->name;
                $path = $this->path . $name . "." . $this->extension;

                $public_path = public_path($path);
                $file_exists = file_exists($public_path);

                if ($file_exists) {
                    return asset($path);
                }
            }
        } catch (Exception $ex) {
            _systemLog($ex, "error");
        }

        return null;
    }

    /**
     * STATIC
     */
    public static function getStatusList()
    {
        return [
            "Inactive", "Active",
        ];
    }
}
