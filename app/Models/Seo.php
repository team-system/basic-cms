<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelTraits;
use Illuminate\Database\Eloquent\SoftDeletes;


class Seo extends Model
{
    use ModelTraits, SoftDeletes;

    protected $table = 'seo';
}
