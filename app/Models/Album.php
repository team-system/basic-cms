<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelTraits;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Album extends Model
{
    use SoftDeletes, ModelTraits;

    CONST DESTINATION_PATH = 'images/album/';

    /**
     * [RELATIONSHIPS]
     */
    function galleries()
    {
        return $this->morphMany('App\Models\Media', 'parent');
    }
}
