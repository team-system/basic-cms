<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelTraits;
use Illuminate\Database\Eloquent\SoftDeletes;


class Blog extends Model
{
    use ModelTraits, SoftDeletes;
}
