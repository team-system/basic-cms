<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelTraits;
use Illuminate\Database\Eloquent\SoftDeletes;


class Media extends Model
{
    use ModelTraits, SoftDeletes;

    const NEW_FILE_PATH = "storage/uploads/";

    /**
     * [RELATIONSHIPS]
     */
    function mediable()
    {
        return $this->morphTo();
    }

    function albums()
    {
        return $this->morphToMany('App\Models\Album', "parent");
    }
}
