@extends('CMS.layouts.app')
@section('head')

@endsection

@section('content')

<!-- Main content -->
<section class="content">
  @include('CMS.components.messages')
  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Create SEO</h3>
    </div>
    <div class="card-body">
      <form id="submit-form" action="{{ route('seo.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <label for="page_url">Page URL</label>
          <input type="text" class="form-control" name="page_url" placeholder="Page URL" value="{{ old('page_url') }}" />
        </div>
        <div class="form-group">
          <label for="type">Type</label>
          <input type="text" class="form-control" name="type" placeholder="Type (eg. website, music, video, article, book, profile)" value="{{ old('type') }}" />
        </div>
        <div class="form-group">
          <label for="title">Title</label>
          <input type="text" class="form-control" name="title" placeholder="Title" value="{{ old('title') }}" />
        </div>
        <div class="form-group">
          <label for="keywords">Keywords</label>
          <input type="text" class="form-control" name="keywords" placeholder="Keywords" value="{{ old('keywords') }}" />
        </div>
        <div class="form-group">
          <label for="description">Description</label>
          <textarea class="form-control" name="description" placeholder="Description">{{ old('description') }}</textarea>
        </div>
        <div class="form-group">
          <label for="image">Image</label>
          <input type="file" class="form-control image_input" name="image" placeholder="Image" />
          <p class="text-muted">
            <small>
            Acceptable formats: jpg, jpeg, png, gif<br>
            Max file size is 7MB/file.
            </small>
          </p>
          <div class="preview_container" style="display: none">
            <img style="max-width: 200px" class="img-fluid">
          </div>
        </div>
        <button type="submit" class="d-none"></button>
      </form>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
      <a href="{{ route('seo.index') }}" class="btn btn-warning">Back</a>
      <button class="btn btn-success btn-submit">Confirm</button>
    </div>
    <!-- /.card-footer-->
  </div>
  <!-- /.card -->

</section>
<!-- /.content -->



@stop
@section('script')
<script>
  
  $('.btn-submit').on('click',function(){
    $('#submit-form').find('button[type=submit]').trigger('click');
  });

</script>
@endsection