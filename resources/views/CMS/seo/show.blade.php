@extends('CMS.layouts.app')
@section('head')

@endsection

@section('content')

<!-- Main content -->
<section class="content">
  @include('CMS.components.messages')
  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">View SEO
        <a href="{{ route('seo.index') }}" class="btn-sm btn-primary">Back</a>
        <a href="{{ route('seo.edit',['seo' => $seo->id]) }}" class="btn-sm btn-warning">Edit</a>
      </h3>
    </div>
    <div class="card-body">

        @csrf

        <div class="form-group">
          <label for="page_url">Page URL</label>
          <p>{{ $seo->page_url }}</p>
        </div>
        <div class="form-group">
          <label for="type">Type</label>
          <p>{{ $seo->type }}</p>
        </div>
        <div class="form-group">
          <label for="title">Title</label>
          <p>{{ $seo->title }}</p>
        </div>
        <div class="form-group">
          <label for="keywords">Keywords</label>
          <p>{!! $seo->keywords !!}</p>
        </div>
        <div class="form-group">
          <label for="description">Description</label>
          <p>{!! $seo->description !!}</p>
        </div>
        <div class="form-group">
          <label for="image">Image</label>
          @if($seo->image != '')
          <div class="preview_container">
              <img style="max-width: 200px" class="img-fluid" src="{{ asset($seo->image) }}">
          </div>
          @else
          <p>No Image</p>
          @endif
      </div>

    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->

</section>
<!-- /.content -->




@stop
@section('script')

@endsection