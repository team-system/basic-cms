@extends('CMS.layouts.app')
@section('head')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('CMS/plugins/adminlte/datatables/dataTables.bootstrap4.css') }}">
@endsection

@section('content')

<!-- Main content -->
<section class="content">
    @include('CMS.components.messages')
    <!-- Default box -->
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Create SEO
                <a href="{{ route('seo.create') }}" class="btn-sm btn-success">Create</a>
            </h3>
        </div>
        <div class="card-body">
            <table id="index-table" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Page URL</th>
                        <th>Title</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($seo_list as $seo)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $seo->page_url }}</td>
                            <td>{{ $seo->title }}</td>
                            <td>
                                <a class="btn btn-primary" href="{{ route('seo.show',['seo'=>$seo->id]) }}"><i class="fa fa-search" aria-hidden="true"></i></a>
                                <a class="btn btn-warning" href="{{ route('seo.edit',['seo'=>$seo->id]) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                <button class="btn btn-danger delete_link" href="{{ route('seo.destroy',['seo'=>$seo->id]) }}" data-name="SEO"><i class="fa fa-trash" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>No.</th>
                        <th>Page URL</th>
                        <th>Title</th>
                        <th>Actions</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.card-body -->

    </div>
    <!-- /.card -->

</section>
<!-- /.content -->




@stop
@section('script')
<!-- DataTables -->
<script src="{{ asset('CMS/plugins/adminlte/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('CMS/plugins/adminlte/datatables/dataTables.bootstrap4.js') }}"></script>
<script>
    $('#index-table').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false
    });
</script>
@endsection