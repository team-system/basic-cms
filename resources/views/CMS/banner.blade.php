@extends('CMS.layouts.app')

@section('head')
    <!-- jQuery -->
    <script src="{{ asset('CMS/plugins/jquery/dist/jquery.js') }}"></script>
    <!-- Bootstrap 3-->
    <script src="{{ asset('CMS/plugins/js/modal.js?' . env('APP_VERSION')) }}"></script>
    <!-- Bootbox -->
    <script src="{{ asset('CMS/plugins/bootbox/bootbox.min.js') }}"></script>
    <!-- Dropzone -->
    <link rel="stylesheet" href="{{asset('CMS/plugins/dropzone/dropzone.css')}}">
    <script src="{{ asset('CMS/plugins/dropzone/dropzone.js') }}"></script>
    <!-- Fancybox -->
    <link href="{{ asset('CMS/plugins/fancybox-master/dist/jquery.fancybox.css') }}" rel="stylesheet"/>
    <script src="{{ asset('CMS/plugins/fancybox-master/dist/jquery.fancybox.js') }}"></script>
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{asset('css/banner.css?' . env('APP_VERSION'))}}">
@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Banner Listing</h3>
        </div>
        <div class="box-body">
            <div id="enquiry-table_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <form method="POST" action="{{ route('banner.store') }}" class="dropzone record_dropzone" enctype="multipart/form-data" style="width:100%;">
                    {{ csrf_field() }}
                </form>
                @if (count($results) > 0)
                <div class="col-sm-12">
                    <div class="row">
                        @foreach($results as $banner)
                            <div class="col-md-3 same-height">
                                <div class="record-item">
                                    <img data-fancybox="gallery" href="{{ asset($banner->img_url) }}" src="{{ asset($banner->img_url) }}" class="record-image" />
    
                                    <div class="overlay">
                                        <div class="control record-modify" data-action-url="{{ route('banner.update', $banner->id) }}" data-seq="{{$banner->seq}}" data-title="{{$banner->title}}" data-desc="{{$banner->desc}}" data-link="{{$banner->link}}" data-id="{{$banner->id}}"><i class="fa fa-edit"></i></div>
                                        <div class="control record-delete" data-action-url="{{ route('banner.destroy', $banner->id) }}"><i class="fa fa-trash"></i></div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('script')
<script>
    Dropzone.autoDiscover = false;

    $(document).ready(function(){
        @if(Session::has("success"))
            bootbox.alert({
                message: '<b>{{ Session::get("success") }}</b>'
            });
        @endif

        @if(count($errors->formerror->all()) > 0)
            bootbox.alert({
                message: "<b>Please resolve the following errors:</b></br>"
                    @foreach($errors->formerror->all() as $error)
                        + "</br> {{ $error }}"
                    @endforeach
            });
        @endif

        var myDropzone = new Dropzone(".record_dropzone", {
            url: "{{ route('banner.store') }}",
            uploadMultiple: true,
            parallelUploads: 10,
            autoProcessQueue: true,
            error: function(file, response) {
                if ($.type(response) === "string") {
                    var message = response;
                } else {
                    var message = response.message;
                }
                $('.dropzone').append(message);
            },
            paramName: "file",
        });

        myDropzone.on("queuecomplete", function(progress) {
            window.location.reload();
        });
    })

	$('.record-delete').on('click', function(e) {
        e.preventDefault()
        var csrfField = document.createElement("input");
        csrfField.setAttribute("type", "hidden");
        csrfField.setAttribute("name", "_token");
		csrfField.setAttribute("value", "{{ csrf_token() }}");
		bootbox.confirm({
			message: "<form class='bootbox-form' id='banner-delete' method='POST'>\
            <input name='_method' type='hidden' value='DELETE'>\
            <p>Are you sure to remove this banner?</p>\
            </form>",
			buttons: {
				confirm: {
					label: 'Yes',
					className: 'btn-success'
				},
				cancel: {
					label: 'No',
					className: 'btn-danger'
				}
			},
			callback: function (result) {
				if (result) {
                    var url = $(e.currentTarget).data('action-url');
				
                    $('#banner-delete').attr('action', url);
                    $('#banner-delete').append(csrfField);
                    $('#banner-delete').submit();
				}
			}
		});
    })	
    
    $('.record-modify').on('click', function(e) {
        e.preventDefault()
        var csrfField = document.createElement("input");
        csrfField.setAttribute("type", "hidden");
        csrfField.setAttribute("name", "_token");
		csrfField.setAttribute("value", "{{ csrf_token() }}");
        var seq = $(this).data('seq');
        var title = $(this).data('title');
        var desc = $(this).data('desc');
        var link = $(this).data('link');
        var banner_id = $(this).data('id');
		
		bootbox.confirm("<form class='bootbox-form' id='seq-edit' method='POST'>\
            <input name='_method' type='hidden' value='PATCH'>\
            <input name='banner_id' type='hidden' value=\"" + banner_id + "\">\
			<label>Sequence</label>\
            <input class='bootbox-input bootbox-input-number form-control' type='number' name='seq' placeholder='Sequence' value=\"" + seq + "\"><br>\
            <label>Title</label>\
            <input class='bootbox-input bootbox-input-number form-control' type='text' name='title' placeholder='Title' value=\"" + title + "\"><br>\
            <label>Description</label>\
            <input class='bootbox-input bootbox-input-number form-control' type='text' name='desc' placeholder='Description' value=\"" + desc + "\"><br>\
            <label>Link</label>\
            <input class='bootbox-input bootbox-input-number form-control' type='text' name='link' placeholder='Link' value=\"" + link + "\"><br>\
        </form>", function(result) {
            if(result) {
				var url = $(e.currentTarget).data('action-url');
				
                $('#seq-edit').attr('action', url);
                $('#seq-edit').append(csrfField);
				$('#seq-edit').submit();
            }
        });
	})
</script>
@endsection
