@extends('CMS.layouts.app')
@section('head')

@endsection

@section('content')

<!-- Main content -->
<section class="content">
  @include('CMS.components.messages')
  <!-- Default box -->
<div class="card">
    <form id="submit-form" action="{{ route('gallery.store') }}" method="post">
        <div class="card-header">
            <h3 class="card-title">Create Album</h3>
        </div>
        <div class="card-body">
            @csrf
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" name="title" placeholder="Title" value="{{ old('title') }}" />
            </div>

            <div class="form-group">
                <label for="content">Description</label>
                <textarea class="form-control" name="description">{!! old('description') !!}</textarea>
            </div>

            <div class="form-group">
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" name="status" id="status" />
                    <label class="form-check-label" for="status">Publish</label>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
        
        <div class="card-footer">
            <a href="{{ route('gallery.index') }}" class="btn btn-warning">Back</a>
            <button class="btn btn-success btn-submit">Confirm</button>
        </div>
        <!-- /.card-footer-->
    </form>
</div>
<!-- /.card -->

</section>
<!-- /.content -->

@stop

@section('script')
@endsection