@extends('CMS.layouts.app')
@section('head')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('CMS/plugins/adminlte/datatables/dataTables.bootstrap4.css') }}">
@endsection

@section('content')

<!-- Main content -->
<section class="content">
    @include('CMS.components.messages')
    <!-- Default box -->
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Create Album
                <a href="{{ route('gallery.create') }}" class="btn-sm btn-success">Create</a>
            </h3>
        </div>
        <div class="card-body">
            <table id="index-table" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Publish</th>
                        <th>Images</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($albums as $album)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $album->name }}</td>
                            <td>{{ strip_tags(Str::limit($album->description,20)) }}</td>
                            <td>
                                {{ $album->publish_text }}
                            </td>
                            <td>
                                {{ $album->galleries->count() }}
                            </td>
                            <td>
                                <a class="btn btn-primary" href="{{ route('gallery.show',['gallery'=>$album->id]) }}"><i class="fa fa-search" aria-hidden="true"></i></a>
                                <a class="btn btn-warning" href="{{ route('gallery.edit',['gallery'=>$album->id]) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                <button class="btn btn-danger delete_link" href="{{ route('gallery.show',['gallery'=>$album->id]) }}" data-name="{{ $album->title }}"><i class="fa fa-trash" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>No.</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Publish</th>
                        <th>Images</th>
                        <th>Actions</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.card-body -->

    </div>
    <!-- /.card -->

</section>
<!-- /.content -->




@stop
@section('script')
<!-- DataTables -->
<script src="{{ asset('CMS/plugins/adminlte/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('CMS/plugins/adminlte/datatables/dataTables.bootstrap4.js') }}"></script>
<script>
    $('#index-table').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false
    });
</script>
@endsection