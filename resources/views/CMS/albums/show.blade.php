@extends('CMS.layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
  @include('CMS.components.messages')
  <!-- Default box -->
<div class="card">
    <div class="card-header">
        <h3 class="card-title">View album
            <a href="{{ route('gallery.index') }}" class="btn-sm btn-primary">Back</a>
            <a href="{{ route('gallery.edit',['gallery' => $album->id]) }}" class="btn-sm btn-warning">Edit</a>
        </h3>
    </div>
    <div class="card-body">
        <div class="form-group">
            <label for="title">Title</label>
            <p>
                {{ $album->name }}
            </p>
        </div>

        <div class="form-group">
            <label for="content">Description</label>
            <p>
                {{ $album->description }}
            </p>
        </div>

        <div class="form-group">
            <label for="content">Publish</label>
            <p>
                {{ $album->publish_text }}
            </p>
        </div>
    </div>
    <!-- /.card-body -->
    
    <div class="card-footer">
        <h3>
            Images
        </h3>
        <div id="images-table_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
            <form method="POST" action="{{ route('gallery.images.store', ['gallery' => $album->id]) }}" class="dropzone record_dropzone" enctype="multipart/form-data" style="width:100%;">
                {{ csrf_field() }}
            </form>
            @if ($album->galleries->count() > 0)
            <div class="col-sm-12">
                <div class="row">
                    @php
                    $results = $album->galleries;   
                    @endphp
                    @foreach($results as $gallery)
                        <div class="col-md-3 same-height">
                            <div class="record-item">
                                <img data-fancybox="gallery" href="{{ asset($gallery->path) }}" src="{{ asset($gallery->path) }}" class="record-image" />

                                <div class="overlay">
                                    <div class="control">
                                        <i class="fa fa-fw fa-eye{{ $gallery->publish ? '' : '-slash' }}" title="{{ $gallery->publish_text }}"></i>
                                    </div>

                                    <div class="control record-modify" data-action-url="{{ route('gallery.images.update', ['gallery' => $album->id, 'media' => $gallery->id]) }}" data-seq="{{$gallery->sequence}}" data-status="{{ $gallery->publish }}" data-id="{{$gallery->id}}"><i class="fa fa-edit"></i></div>
                                    <div class="control record-delete" data-action-url="{{ route('gallery.images.destroy', ['gallery' => $album->id, 'media' => $gallery->id]) }}"><i class="fa fa-trash"></i></div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            @endif
        </div>
    </div>
    <!-- /.card-footer-->
</div>
<!-- /.card -->

</section>
<!-- /.content -->

@stop

@section('head')
    <!-- jQuery -->
    <script src="{{ asset('CMS/plugins/jquery/dist/jquery.js') }}"></script>
    <!-- Bootstrap 3-->
    <script src="{{ asset('CMS/plugins/js/modal.js?' . env('APP_VERSION')) }}"></script>
    <!-- Bootbox -->
    <script src="{{ asset('CMS/plugins/bootbox/bootbox.min.js') }}"></script>
    <!-- Dropzone -->
    <link rel="stylesheet" href="{{asset('CMS/plugins/dropzone/dropzone.css')}}">
    <script src="{{ asset('CMS/plugins/dropzone/dropzone.js') }}"></script>
    <!-- Fancybox -->
    <link href="{{ asset('CMS/plugins/fancybox-master/dist/jquery.fancybox.css') }}" rel="stylesheet"/>
    <script src="{{ asset('CMS/plugins/fancybox-master/dist/jquery.fancybox.js') }}"></script>

    <link rel="stylesheet" href="{{asset('css/banner.css?' . env('APP_VERSION'))}}">
@endsection

@section('script')
<script>
    Dropzone.autoDiscover = false;

    $(document).ready(function(){
        @if(Session::has("success"))
            bootbox.alert({
                message: '<b>{{ Session::get("success") }}</b>'
            });
        @endif

        @if(count($errors->formerror->all()) > 0)
            bootbox.alert({
                message: "<b>Please resolve the following errors:</b></br>"
                    @foreach($errors->formerror->all() as $error)
                        + "</br> {{ $error }}"
                    @endforeach
            });
        @endif

        var myDropzone = new Dropzone(".record_dropzone", {
            url: $(".record_dropzone").attr("action"),
            uploadMultiple: true,
            parallelUploads: 10,
            autoProcessQueue: true,
            error: function(file, response) {
                if ($.type(response) === "string") {
                    var message = response;
                } else {
                    var message = response.message;
                }
                $('.dropzone').append(message);
            },
            paramName: "file",
        });

        myDropzone.on("queuecomplete", function(progress) {
            window.location.reload();
        });
    })

	$('.record-delete').on('click', function(e) {
        e.preventDefault()
        var csrfField = document.createElement("input");
        csrfField.setAttribute("type", "hidden");
        csrfField.setAttribute("name", "_token");
		csrfField.setAttribute("value", "{{ csrf_token() }}");
		bootbox.confirm({
			message: "<form class='bootbox-form' id='image-delete' method='POST'>\
            <input name='_method' type='hidden' value='DELETE'>\
            <p>Are you sure to remove this image?</p>\
            </form>",
			buttons: {
				confirm: {
					label: 'Yes',
					className: 'btn-success'
				},
				cancel: {
					label: 'No',
					className: 'btn-danger'
				}
			},
			callback: function (result) {
				if (result) {
                    var url = $(e.currentTarget).data('action-url');
				
                    $('#image-delete').attr('action', url);
                    $('#image-delete').append(csrfField);
                    $('#image-delete').submit();
				}
			}
		});
    });

    $('.record-modify').on('click', function(e) {
        e.preventDefault()
        var csrfField = document.createElement("input");
        csrfField.setAttribute("type", "hidden");
        csrfField.setAttribute("name", "_token");
		csrfField.setAttribute("value", "{{ csrf_token() }}");
        var seq = $(this).data('seq');
        var status = $(this).data("status") == "1" ? "checked" : "";
		
		bootbox.confirm("<form class='bootbox-form' id='seq-edit' method='POST'>\
            <input name='_method' type='hidden' value='PUT'>\
			<label>Sequence</label>\
            <input class='bootbox-input bootbox-input-number form-control' type='number' name='seq' placeholder='Sequence' value=\"" + seq + "\"><br>\
            <label style=\"width:100%\">\
            <input class='bootbox-input bootbox-input-number' type='checkbox' name='status' placeholder='Publish' "+status+
            ">Publish</label>\
        </form>", function(result) {
            if(result) {
				var url = $(e.currentTarget).data('action-url');
				
                $('#seq-edit').attr('action', url);
                $('#seq-edit').append(csrfField);
				$('#seq-edit').submit();
            }
        });
	})
</script>
@endsection