@extends('CMS.layouts.app')
@section('head')

@endsection

@section('content')

<!-- Main content -->
<section class="content">
  @include('CMS.components.messages')
  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Create Blog</h3>
    </div>
    <div class="card-body">
      <form id="submit-form" action="{{ route('blogs.store') }}" method="post">
        @csrf
        <div class="form-group">
          <label for="title">Title</label>
          <input type="text" class="form-control" name="title" placeholder="Title" value="{{ old('title') }}" />
        </div>
        <div class="form-group">
          <label for="content">Content</label>
          <textarea class="form-control" name="content">{!! old('content') !!}</textarea>
        </div>
        <button type="submit" class="d-none"></button>
      </form>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
      <a href="{{ route('blogs.index') }}" class="btn btn-warning">Back</a>
      <button class="btn btn-success btn-submit">Confirm</button>
    </div>
    <!-- /.card-footer-->
  </div>
  <!-- /.card -->

</section>
<!-- /.content -->



@stop
@section('script')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
<script>
  CKEDITOR.replace( 'content', {
      uploadUrl : "{{ route('ckeditor.upload.image',['_token'=>csrf_token()]) }}",
      filebrowserUploadUrl : "{{ route('ckeditor.upload.image',['_token'=>csrf_token()]) }}",
      entities : false,
      fillEmptyBlocks : true
  });
  $('.btn-submit').on('click',function(){
    $('#submit-form').find('button[type=submit]').trigger('click');
  });

</script>
@endsection