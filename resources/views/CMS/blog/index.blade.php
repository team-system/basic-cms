@extends('CMS.layouts.app')
@section('head')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('CMS/plugins/adminlte/datatables/dataTables.bootstrap4.css') }}">
@endsection

@section('content')

<!-- Main content -->
<section class="content">
    @include('CMS.components.messages')
    <!-- Default box -->
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Create Blog
                <a href="{{ route('blogs.create') }}" class="btn-sm btn-success">Create</a>
            </h3>
        </div>
        <div class="card-body">
            <table id="index-table" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Title</th>
                        <th>Content</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($blogs as $blog)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $blog->title }}</td>
                            <td>{{ strip_tags(Str::limit($blog->content,20)) }}</td>
                            <td>
                                <a class="btn btn-primary" href="{{ route('blogs.show',['blog'=>$blog->id]) }}"><i class="fa fa-search" aria-hidden="true"></i></a>
                                <a class="btn btn-warning" href="{{ route('blogs.edit',['blog'=>$blog->id]) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                <button class="btn btn-danger" href="{{ route('blogs.show',['blog'=>$blog->id]) }}"><i class="fa fa-trash" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>No.</th>
                        <th>Title</th>
                        <th>Content</th>
                        <th>Actions</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.card-body -->

    </div>
    <!-- /.card -->

</section>
<!-- /.content -->




@stop
@section('script')
<!-- DataTables -->
<script src="{{ asset('CMS/plugins/adminlte/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('CMS/plugins/adminlte/datatables/dataTables.bootstrap4.js') }}"></script>
<script>
    $('#index-table').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false
    });
</script>
@endsection