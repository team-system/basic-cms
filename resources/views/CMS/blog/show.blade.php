@extends('CMS.layouts.app')
@section('head')

@endsection

@section('content')

<!-- Main content -->
<section class="content">
  @include('CMS.components.messages')
  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">View Blog
        <a href="{{ route('blogs.index') }}" class="btn-sm btn-primary">Back</a>
        <a href="{{ route('blogs.edit',['blog' => $blog->id]) }}" class="btn-sm btn-warning">Edit</a>
      </h3>
    </div>
    <div class="card-body">

        @csrf
        <div class="form-group">
          <label for="title">Title</label>
          <p>{{ $blog->title }}</p>
        </div>
        <div class="form-group">
          <label for="content">Content</label>
          {!! $blog->content !!}
        </div>
        <button type="submit" class="d-none"></button>

    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->

</section>
<!-- /.content -->




@stop
@section('script')

@endsection