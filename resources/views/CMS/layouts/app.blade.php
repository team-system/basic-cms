<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ETC | CMS</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('CMS/plugins/adminlte/font-awesome/css/font-awesome.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('CMS/plugins/adminlte/adminlte.min.css') }}">
    {{-- <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('CMS/plugins/adminlte/iCheck/flat/blue.css') }}">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{ asset('CMS/plugins/adminlte/morris/morris.css') }}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{ asset('CMS/plugins/adminlte/jvectormap/jquery-jvectormap-1.2.2.css') }}">
    <!-- Date Picker -->
    <link rel="stylesheet" href="{{ asset('CMS/plugins/adminlte/datepicker/datepicker3.css') }}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{ asset('CMS/plugins/adminlte/daterangepicker/daterangepicker-bs3.css') }}">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{ asset('CMS/plugins/adminlte/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}"> --}}
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('CMS/css/styles.css?v='.date('YmdHis')) }}">

    @yield('head')

    @yield('styles')
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        @include('CMS.layouts.topnav')

        @include('CMS.layouts.sidenav')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            @yield('content')

        </div>
        <!-- /.content-wrapper -->

        @include('CMS.layouts.footer')

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="{{ asset('CMS/plugins/adminlte/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('CMS/plugins/adminlte/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- Slimscroll -->
    <script src="{{ asset('CMS/plugins/adminlte/slimScroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('CMS/plugins/adminlte/fastclick/fastclick.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('CMS/plugins/adminlte/adminlte.js') }}"></script>
    <!-- Bootbox -->
    <script src="{{ asset('CMS/js/bootbox.all.min.js') }}"></script>
    <!-- Custom Script -->
    <script src="{{ asset('CMS/js/script.js?v='.date('YmdHis')) }}"></script>

    @yield('script')
    {{--
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="{{ asset('CMS/plugins/adminlte/morris/morris.min.js') }}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Sparkline -->
    <script src="{{ asset('CMS/plugins/adminlte/sparkline/jquery.sparkline.min.js') }}"></script>
    <!-- jvectormap -->
    <script src="{{ asset('CMS/plugins/adminlte/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/adminlte/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{ asset('CMS/plugins/adminlte/knob/jquery.knob.js') }}"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="{{ asset('CMS/plugins/adminlte/daterangepicker/daterangepicker.js') }}"></script>
    <!-- datepicker -->
    <script src="{{ asset('CMS/plugins/adminlte/datepicker/bootstrap-datepicker.js') }}"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{ asset('CMS/plugins/adminlte/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
    --}}
</body>
</html>
